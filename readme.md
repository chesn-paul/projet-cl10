# CL10

GitLab du projet de CL10

## Arborescence

- un dossier 'Sujet' avec les instructions d'utilisation et le sujet.
- un dossier 'Projet' qui contient les fichiers .py pour chaque question. Dans ce dossier il y a également le dossier 'Instance' qui contient les différentes instances à tester.

## Travail à faire

- check du code du projet suite aux remarques de Murat
- rapport du projet (idée : étude statistique pour l'analyse des résultats: fichier à faire .py)
- ppt pour l'article

# Liens

* Rapport : https://docs.google.com/document/d/1qKT6cjs2OO7cG8F_kuB6_HBABayN8Zo4lR5u1wbt_gs/edit?fbclid=IwAR1s5R1EVINbyO3cLu6XqkNG3n57Zu5SZ4oF_lteN17Po9h8Xt0BT8EWeAo
* Collab : https://colab.research.google.com/drive/1ng-t5QaKMBP7TwXRQEFQJvSgXhbMa5s-?usp=sharing&fbclid=IwAR3vDkIp-DXqTcKUmozCrRkbF7X4QVDWZJoJM5DjJJX_369q7SB0I1p2SIQ
